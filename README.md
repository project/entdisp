# EntDisP ("Entity Display Handler")

Entdisp is a plugin system for entity display handlers, based on the [CfrPlugin](https://github.com/donquixote/drupal-cfr) API.

An "entity display handler" is an object that builds render arrays from entities.
The interface, `Drupal\renderkit\EntityDisplay\EntityDisplayInterface`, and most implementations, live in the [Renderkit](https://github.com/donquixote/drupal-renderkit) module.

EntDisP allows to use these components in different places in a Drupal site.

## Integration

Integration of EntityDisplay* components:

* A views row plugin, [EntdispViewsRowPlugin](src/ViewsPlugin/row/EntdispViewsRowPlugin.php).
* A views field handler, [EntdispViewsFieldHandler](src/ViewsPlugin/field/EntdispViewsFieldHandler.php).
* A field formatter for different reference fields, see [entdisp.field.inc](entdisp.field.inc).

Integration of EntitiesListFormat* components:

* A views style plugin, [ViewsStylePlugin_EntitiesListFormat](src/ViewsPlugin/style/ViewsStylePlugin_EntitiesListFormat.php).
* A field formatter for different reference fields, see [entdisp.field.inc](entdisp.field.inc).
