<?php

namespace Drupal\entdisp\ViewsPlugin\style;

use Drupal\cfrapi\SummaryBuilder\SummaryBuilder_Static;
use Drupal\renderkit\EntitiesListFormat\EntitiesListFormatInterface;

class ViewsStylePlugin_EntitiesListFormat extends ViewsStylePluginBase {

  /**
   * @var string|null
   */
  private $base_table;

  /**
   * @var string|null
   */
  private $base_field;

  /**
   * @var string|null
   */
  private $field_alias;

  /**
   * @var string|null
   */
  private $entityType;

  /**
   * @param \view $view
   * @param \views_display $display
   * @param array|null $options
   *
   * @see \entity_views_plugin_row_entity_view::init()
   */
  public function init(&$view, &$display, $options = NULL) {
    parent::init($view, $display, $options);

    // Initialize the entity-type used.
    $table_data = views_fetch_data($this->view->base_table);

    $this->entityType = $table_data['table']['entity type'];

    // Set base table and field information as used by views_plugin_row to
    // select the entity id if used with default query class.
    $info = entity_get_info($this->entityType);
    if (!empty($info['base table']) && $info['base table'] === $this->view->base_table) {
      // The base class, \views_plugin_row, does not properly declare all properties.
      /** @noinspection PhpUndefinedFieldInspection */
      $this->base_table = $info['base table'];
      /** @noinspection PhpUndefinedFieldInspection */
      $this->base_field = $info['entity keys']['id'];
    }
  }

  /**
   * Set default options
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['entitiesListFormat'] = ['default' => []];

    return $options;
  }

  /**
   * Render the given style.
   *
   * @param array $form
   * @param array $form_state
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['entitiesListFormat'] = [
      '#type' => 'cfrplugin',
      '#cfrplugin_interface' => EntitiesListFormatInterface::class,
      '#title' => t('Entities list format'),
      '#default_value' => $this->options['entitiesListFormat'],
    ];
  }

  /**
   * Returns the summary of the settings in the display.
   */
  public function summary_title() {
    return $this->getConfigurator()->confGetSummary(
      $this->options['entitiesListFormat'],
      new SummaryBuilder_Static());
  }

  /**
   * Modify the query.
   *
   * @see \views_plugin_row::query()
   */
  public function query() {

    if (!isset($this->base_table)) {
      return;
    }

    $query = $this->view->query;

    if (!method_exists($query, 'add_field')) {
      return;
    }

    if (isset(
      $this->options['relationship'],
      $this->view->relationship[$this->options['relationship']]
    )) {
      $relationship = $this->view->relationship[$this->options['relationship']];
      $this->field_alias = $query->add_field($relationship->alias, $this->base_field);
    }
    else {
      $this->field_alias = $query->add_field($this->base_table, $this->base_field);
    }
  }

  /**
   * @param string[] $rows
   * @param string $title
   * @param int $grouping_level
   *
   * @return string
   */
  protected function renderRows(array $rows, $title, $grouping_level) {

    try {
      $entities = $this->rowsGetEntities($rows);

      $build = $this->getEntitiesListFormat()->entitiesBuildList($this->entityType, $entities);

      return drupal_render($build);
    }
    catch (\Exception $e) {
      watchdog('cfrplugin',
        'Broken EntitiesListFormat plugin in Views style plugin for @view_name/@display_id.',
        [
          '@view_name' => $this->view->name,
          '@display_id' => $this->view->current_display,
        ],
        WATCHDOG_WARNING);

      return '';
    }
  }

  /**
   * @return \Drupal\renderkit\EntitiesListFormat\EntitiesListFormatInterface
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  private function getEntitiesListFormat() {
    return $this->getConfigurator()->confGetValue($this->options['entitiesListFormat']);
  }

  /**
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private function getConfigurator() {
    return cfrplugin()->interfaceGetConfigurator(EntitiesListFormatInterface::class);
  }

  /**
   * @param object[] $rows
   *
   * @return object[]
   */
  private function rowsGetEntities(array $rows) {

    if (empty($this->entityType)) {
      // @todo Show a warning / log to watchdog.
      return [];
    }

    $relationship = !empty($this->relationship) ? $this->relationship : NULL;
    $field_alias = isset($this->field_alias) ? $this->field_alias : NULL;

    // Some views query classes want/allow a third parameter specifying the field name.
    /** @noinspection PhpMethodParametersCountMismatchInspection */
    list($entityType, $entities) = $this->view->query->get_result_entities($rows, $relationship, $field_alias);

    if (empty($entityType)) {
      // @todo Show a warning / log to watchdog.
      return [];
    }

    if ($entityType !== $this->entityType) {
      // @todo Show a warning / log to watchdog.
      return [];
    }

    return $entities;
  }

}
